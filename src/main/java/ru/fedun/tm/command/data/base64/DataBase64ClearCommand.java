package ru.fedun.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.constant.DataConstant;
import ru.fedun.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;

public final class DataBase64ClearCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-clear-base64";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear data from base64 file.";
    }

    @Override
    public void execute() throws Exception{
        System.out.println("[DATA BASE64 CLEAR]");
        @NotNull final File file = new File(DataConstant.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
        System.out.println();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
