package ru.fedun.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.constant.DataConstant;
import ru.fedun.tm.dto.Domain;
import ru.fedun.tm.enumerated.Role;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public final class DataJsonFasterXmlSaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-save-json-fx";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to json (fasterxml) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON (FASTERXML) SAVE]");
        @NotNull final File file = new File(DataConstant.FILE_JSON_FX);
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(DataConstant.FILE_JSON_FX);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
        System.out.println("[OK]");
        System.out.println();
    }

    @NotNull
    @Override
    public Role[] roles(){
        return new Role[]{Role.ADMIN};
    }

}
