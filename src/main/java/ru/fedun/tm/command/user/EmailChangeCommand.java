package ru.fedun.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.util.TerminalUtil;

public final class EmailChangeCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "change-email";
    }

    @NotNull
    @Override
    public String description() {
        return "Change profile e-mail.";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE EMAIL]");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER NEW EMAIL:]");
        @Nullable final String newEmail = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateMail(userId, newEmail);
        System.out.println("[OK]");
        System.out.println();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN, Role.USER };
    }

}
