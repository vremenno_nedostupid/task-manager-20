package ru.fedun.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.util.TerminalUtil;

public final class PasswordChangeCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "change-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Change profile password.";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER OLD PASSWORD:]");
        @Nullable final String oldPassword = TerminalUtil.nextLine();
        System.out.println("[ENTER NEW PASSWORD:]");
        @Nullable final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getUserService().updatePassword(userId, oldPassword, newPassword);
        System.out.println("[OK]");
        System.out.println();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN, Role.USER };
    }

}
