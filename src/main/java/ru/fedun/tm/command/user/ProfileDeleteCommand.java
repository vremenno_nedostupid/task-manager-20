package ru.fedun.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.util.TerminalUtil;

public final class ProfileDeleteCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "delete-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete profile.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DELETE PROFILE]");
        System.out.println("DO YOU REALLY WANT DELETE PROFILE?");
        System.out.println("1 - YES, 2 - NO");
        @Nullable final Integer answer = TerminalUtil.nextInt();
        if (answer == null) return;
        if (answer.equals(1)) {
            @NotNull final String userId = serviceLocator.getAuthService().getUserId();
            @NotNull final User user = serviceLocator.getUserService().findById(userId);
            serviceLocator.getUserService().removeUser(userId, user);
        } else return;
        System.out.println("[OK]");
        System.out.println();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
