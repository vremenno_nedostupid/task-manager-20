package ru.fedun.tm.command.project.remove;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by index.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextInt();
        if (index <= 0) {
            System.out.println("[FAIL]");
            return;
        }
        serviceLocator.getProjectService().removeOneByIndex(userId, index);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
