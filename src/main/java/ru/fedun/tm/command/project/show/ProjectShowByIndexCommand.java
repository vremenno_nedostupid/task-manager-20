package ru.fedun.tm.command.project.show;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.exception.user.AccessDeniedException;
import ru.fedun.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-view_by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by index.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextInt() - 1;
        @NotNull final Project project = serviceLocator.getProjectService().getOneByIndex(userId, index);
        System.out.println("ID: " + project.getId());
        System.out.println("TITLE: " + project.getTitle());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
        System.out.println();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN, Role.USER };
    }

}
