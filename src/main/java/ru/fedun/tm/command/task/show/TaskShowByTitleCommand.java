package ru.fedun.tm.command.task.show;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.exception.user.AccessDeniedException;
import ru.fedun.tm.util.TerminalUtil;

public final class TaskShowByTitleCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-view-by-title";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by name.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER TITLE:]");
        @NotNull final String title = TerminalUtil.nextLine();
        @NotNull final Task task = serviceLocator.getTaskService().getOneByTitle(userId, title);
        System.out.println("ID: " + task.getId());
        System.out.println("TITLE: " + task.getTitle());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
        System.out.println();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN, Role.USER };
    }

}
