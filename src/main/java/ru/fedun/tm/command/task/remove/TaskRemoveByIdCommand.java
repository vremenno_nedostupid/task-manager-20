package ru.fedun.tm.command.task.remove;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by_id";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        if (id.isEmpty()) {
            System.out.println("[FAIL]");
            return;
        }
        serviceLocator.getTaskService().removeOneById(userId, id);
        System.out.println("[OK]");
        System.out.println();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN, Role.USER };
    }

}
