package ru.fedun.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.enumerated.Role;

import java.io.Serializable;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public final class User extends AbstractEntity implements Serializable {

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @NotNull
    private String email;

    @NotNull
    private String firstName;

    @NotNull
    private String secondName;

    @NotNull
    private Role role = Role.USER;

    @NotNull
    private Boolean locked = false;

}
