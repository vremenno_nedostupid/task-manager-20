package ru.fedun.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.fedun.tm.api.repository.ICommandRepository;
import ru.fedun.tm.command.AbstractCommand;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        init();
    }

    @SneakyThrows
    private void init() {
        @NotNull final Reflections reflections = new Reflections("ru.fedun.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.fedun.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            commandList.add(clazz.newInstance());
        }
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return commandList;
    }

}
