package ru.fedun.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.repository.IUserRepository;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.exception.notfound.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public void remove(@NotNull final User user) {
        entities.remove(user);
    }

    @NotNull
    @Override
    public User findById(@NotNull final String id) {
        for (@NotNull final User user : entities) {
            if (id.equals(user.getId())) {
                return user;
            }
        }
        throw new UserNotFoundException();
    }

    @NotNull
    @Override
    public User findByLogin(@NotNull final String login) {
        for (@NotNull final User user : entities) {
            if (login.equals(user.getLogin())) {
                return user;
            }
        }
        throw new UserNotFoundException();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final User user = findById(id);
        remove(user);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        @NotNull final User user = findByLogin(login);
        remove(user);
    }

}
