package ru.fedun.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.repository.IProjectRepository;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.exception.empty.EmptyUserIdException;
import ru.fedun.tm.exception.notfound.ProjectNotFound;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public void add(@NotNull final String userId, @NotNull final Project project) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        project.setUserId(userId);
        entities.add(project);
    }

    public void remove(@NotNull final String userId, @NotNull final Project project) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        entities.remove(project);
    }

    public @NotNull List<Project> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final List<Project> result = new ArrayList<>();
        for (@NotNull final Project project : entities) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @SneakyThrows
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final List<Project> result = findAll(userId);
        entities.removeAll(result);
    }

    @NotNull
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        for (@NotNull final Project project : entities) {
            if (id.equals(project.getId())) return project;
        }
        throw new ProjectNotFound();
    }

    @NotNull
    @Override
    public Project findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        return entities.get(index);
    }

    @NotNull
    @Override
    public Project findOneByTitle(@NotNull final String userId, @NotNull final String title) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        for (final Project project : entities) {
            if (title.equals(project.getTitle())) return project;
        }
        throw new ProjectNotFound();
    }

    @NotNull
    @Override
    public Project removeOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Project project = findOneById(userId, id);
        remove(userId, project);
        return project;
    }

    @NotNull
    @Override
    public Project removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Project project = findOneByIndex(userId, index);
        remove(userId, project);
        return project;
    }

    @NotNull
    @Override
    public Project removeOneByTitle(@NotNull final String userId, @NotNull final String title) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Project project = findOneByTitle(userId, title);
        remove(userId, project);
        return project;
    }

}
