package ru.fedun.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.service.IProjectService;
import ru.fedun.tm.api.service.ITaskService;
import ru.fedun.tm.api.service.IDomainService;
import ru.fedun.tm.api.service.IUserService;
import ru.fedun.tm.dto.Domain;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.entity.Task;

@AllArgsConstructor
public final class DomainService implements IDomainService {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IUserService userService;

    @Override
    public void load(@NotNull final Domain domain) {
        taskService.load(domain.getTasks());
        projectService.load(domain.getProjects());
        userService.load(domain.getUsers());
    }

    @Override
    public void export(@NotNull final Domain domain) {
        domain.setProjects(projectService.findAll());
        domain.setTasks(taskService.findAll());
        domain.setUsers(userService.findAll());
    }

}
