package ru.fedun.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.api.service.IAuthService;
import ru.fedun.tm.api.service.IUserService;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.exception.empty.EmptyLoginException;
import ru.fedun.tm.exception.empty.EmptyPassException;
import ru.fedun.tm.exception.user.AccessDeniedException;
import ru.fedun.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void login(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPassException();
        @NotNull final User user = userService.findByLogin(login);
        if (user.getLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String email
    ) {
        userService.create(login, password, firstName, lastName, email);
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void checkRole(@Nullable final Role[] roles) {
        if (roles == null || roles.length == 0) return;
        final String userId = getUserId();
        @NotNull final User user = userService.findById(userId);
        @NotNull final Role role = user.getRole();
        for (@NotNull final Role item : roles) if (role.equals(item)) return;
        throw new AccessDeniedException();
    }

}
