package ru.fedun.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.fedun.tm.api.repository.ICommandRepository;
import ru.fedun.tm.api.repository.IProjectRepository;
import ru.fedun.tm.api.repository.ITaskRepository;
import ru.fedun.tm.api.repository.IUserRepository;
import ru.fedun.tm.api.service.*;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.exception.empty.EmptyCommandException;
import ru.fedun.tm.repository.CommandRepository;
import ru.fedun.tm.repository.ProjectRepository;
import ru.fedun.tm.repository.TaskRepository;
import ru.fedun.tm.repository.UserRepository;
import ru.fedun.tm.service.*;
import ru.fedun.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(taskService, projectService, userService);

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @SneakyThrows
    private void init() {
        @NotNull final Reflections reflections = new Reflections("ru.fedun.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.fedun.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    private void initUsers() {
        userService.create("test", "test", "Ivan", "Ivanov");
        userService.create("admin", "admin", "Petr", "Petrov", Role.ADMIN);
    }

    @SneakyThrows
    public void run(final String[] args) {
        init();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println();
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            runWithCommand(TerminalUtil.nextLine());
        }
    }

    @SneakyThrows
    private void runWithCommand(@NotNull final String cmd) {
        if (cmd.isEmpty()) throw new EmptyCommandException();
        @NotNull final AbstractCommand command = commands.get(cmd);
        authService.checkRole(command.roles());
        command.execute();
    }

    @SneakyThrows
    public boolean parseArgs(@NotNull final String[] args) {
        if (args.length == 0) return false;
        @NotNull final String arg = args[0];
        runWithArg(arg);
        return true;
    }

    @SneakyThrows
    private void runWithArg(@NotNull final String arg) {
        if (arg.isEmpty()) return;
        @NotNull final AbstractCommand argument = arguments.get(arg);
        argument.execute();
    }

    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    public IDomainService getDomainService() {
        return domainService;
    }

}
