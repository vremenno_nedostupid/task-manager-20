package ru.fedun.tm.exception.empty;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class EmptyPassException extends AbstractRuntimeException {

    private final static String message = "Error! Password is empty...";

    public EmptyPassException() {
        super(message);
    }
}
