package ru.fedun.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    @NotNull
    E add(@NotNull E e);

    void add(@NotNull final List<E> es);

    void add(@NotNull final E... es);

    void clear();

    void load(@NotNull final E... es);

    void load(@NotNull final List<E> es);

}
