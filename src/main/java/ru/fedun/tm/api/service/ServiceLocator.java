package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.entity.Task;

public interface ServiceLocator {

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IDomainService getDomainService();

}
