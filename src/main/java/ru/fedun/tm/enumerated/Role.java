package ru.fedun.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {

    ADMIN("Администратор"),

    USER("Пользователь");

    @NotNull
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
